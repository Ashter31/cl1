# Naive Bayes Classification
# Works for the specific input, Predicts the class yes or no for person buying the computer
# Author -- Aarsh Patil
# Version - 1.0

# Table for classification
classificationTable = []
yes = 0
no = 0

# function to accept input for the table
def inputTableContent():

    print "Enter Age, Income, Student, Credit Rating, Buys computer in following format:: "
    print "--------------------------------------------------------------------"
    print "Age : youth, middle-age, senior"
    print "Income : low, medium, high"
    print "Student : yes or no"
    print "Credit Rating : Fair or Excellent"
    print "Buys computer : yes or no"
    print "--------------------------------------------------------------------"
    print ""

    i = 0
    while i < records:
        classificationTable.append(raw_input().lower().split(" "))
        i += 1


def displayTableContent():
    print '%12s'%"Age","\t",'%8s'%"Income","\t",'%8s'%"Student","\t",'%10s'%"Credit rate","\t",'%10s'%"Buys Computer"
    for record in classificationTable:
        print '%12s'%record[0],"\t", '%8s'%record[1],"\t", '%8s'%record[2],"\t", '%10s'%record[3],"\t", '%10s'%record[4]

def inputQuestion():
    return raw_input("Enter Age, Income, Student, Credit Rating :: ").lower().split(" ")

def calc(parameter,column,yesorno):
    count = 0
    for rec in classificationTable:
        if (rec[column] == parameter and rec[4] == yesorno):
            count += 1
    return count

def calculatingProbability(question):
    # calculating number of yes and no
    global yes
    yes = 0
    global no
    no = 0
    for rec in classificationTable:
        if rec[4] == 'yes':
            yes += 1
        else:
            no += 1

    # calculating probabilities
    p_yes = (calc(question[0], 0, 'yes') * calc(question[1], 1, 'yes') * calc(question[2], 2, 'yes') * calc(question[3], 3, 'yes') * yes) / ((yes * yes * yes * yes * records) * 1.0)
    p_no = (calc(question[0], 0, 'no') * calc(question[1], 1, 'no') * calc(question[2], 2, 'no') * calc(question[3], 3, 'no') * no) / ((no * no * no * no * records) * 1.0)

    print "P (Buys Computer=Yes) = ",p_yes
    print "P (Buys Computer=No) = ",p_no

    if p_yes >= p_no:
        print "Person buys the Computer"
    else:
        print "Person does not buy the Computer"

# main function
records = int(raw_input("No of Records :: "))

# functions
inputTableContent()
displayTableContent()
question = inputQuestion()
calculatingProbability(question)

