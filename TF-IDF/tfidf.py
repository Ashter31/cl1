# Term Frequency - Inverse Document Frequency Algorithm
# For Ranking documents and pages according to the relevant information contained in them
# Author -- Aarsh Patil
# Version - 1.0

#!/bin/python
import math

def calculateTermFrequency(term,document):
    file = open(document,"r")
    docdata = file.read().split(" ")
    file.close()
    return docdata.count(term.lower()) / float(len(docdata))

def calculateInverseDocumentFrequency(term):
    count = 0
    for doc in documents:
        file = open(doc,"r")
        docdata = file.read().split(" ")
        file.close()
        count += docdata.count(term.lower())

    if count > 0:
        return 1.0 + math.log(float(len(documents)) / count)
    else:
        return 1.0

def refactor(array):
    i = 0
    temp = []
    while i < len(documents):
        temp.append(array.pop(0))
        i += 1
    return temp

def displayTFIDF():
    for word in query:
        print word,"\t",tfidf[query.index(word)][0],"\t",tfidf[query.index(word)][1],"\t",tfidf[query.index(word)][2]
        
# Input from the user for names of the documents
documents = raw_input("Enter document names:: ").split(" ")
query = raw_input("Enter Query:: ").lower().split(" ")
data = []
tf = []
idf = []
tfidf = []

# calculating term frequency for the query words
for word in query:
    for doc in documents:
        tf.append(calculateTermFrequency(word,doc))

# refactoring / reorganizing the array for calculation purpose
for i in range(0,len(query)):
    tf.append(refactor(tf))

# calculating the Inverse Document Frequency for query words
for word in query:
    idf.append(calculateInverseDocumentFrequency(word))

# calculating tf-idf values for the query words
i = 0
for idfvalue in idf:
    for j in range(0,3):
        tfidf.append(tf[i][j] * idfvalue)

# again refactor the tfidf array
for k in range(0,len(query)):
    tfidf.append(refactor(tfidf))

displayTFIDF()