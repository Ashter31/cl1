#CL 1 Codes

Naive Bayes Input:

Age Income Student Credit Buys_Comp
------------------------------------
youth high no fair no
youth high no excellent no
middle-age high no fair yes
senior medium no fair yes
senior low yes fair yes
senior low yes excellent no
middle-age low yes excellent yes
youth medium no fair no
youth low yes fair yes
senior medium yes fair yes
youth medium yes excellent yes
middle-age medium no excellent yes
middle-age high yes fair yes
senior medium no excellent no
------------------------------------

